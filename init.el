;; -*- lexical-binding: t -*-

;; Customize UI.
(when (display-graphic-p)
  (tool-bar-mode 0))
(setq inhibit-startup-screen t)
(column-number-mode 1)

;; Load theme.
(load-theme 'modus-operandi)

;; Interactively do things.
(fido-mode 1)

;; Show stray whitespace.
(setq-default show-trailing-whitespace t)

;; Delete trailing whitespace before saving.
(add-hook `before-save-hook `delete-trailing-whitespace)

;; Consider a period followed by a single space to be the end of a sentence.
(setq sentence-end-double-space nil)

;; Use spaces instead of tabs for indentation.
(setq-default indent-tabs-mode nil)

;; Revert buffers when files are changed externally.
(global-auto-revert-mode 1)

;; Write backups to a separate directory.
(defvar --backup-directory (file-name-as-directory (expand-file-name "backups" user-emacs-directory)))
(if (not (file-exists-p --backup-directory))
    (make-directory --backup-directory t))
(setq backup-directory-alist `((".*" . ,--backup-directory)))
(setq auto-save-file-name-transforms `((".*" ,--backup-directory t)))
(setq auto-save-default nil)

;; Do not move the current file while creating a backup.
(setq backup-by-copying t)

;; Disable lockfiles.
(setq create-lockfiles nil)

;; Single space after sentences.
(setq sentence-end-double-space nil)

;; Write customizations to a separate file.
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file t)

;; Add MELPA to package archives.
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Install use-package.
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(eval-when-compile (require 'use-package))

;; Configure built-in packages.
(use-package dired
  :config
  (put 'dired-find-alternate-file 'disabled nil)
  (setq dired-recursive-deletes 'always)
  (setq dired-recursive-copies 'always))

(use-package elec-pair
  :config
  (electric-pair-mode 1))

(use-package hl-line
  :config
  (global-hl-line-mode 1))

(use-package org
  :hook ((org-mode . visual-line-mode)))

(use-package paren
  :config
  (show-paren-mode 1))

(use-package windmove
  :config
  (windmove-default-keybindings))

(use-package xref
  :config
  (setq xref-search-program 'ripgrep))

(use-package ansi-color
  :hook (compliation-filter . ansi-color-complation-filter))

(use-package eglot
  :config
  (add-to-list 'eglot-server-programs '((typescript-ts-mode tsx-ts-mode) "typescript-language-server" "--stdio")))

;; Configure third-party packages.
(use-package magit
  :ensure t)

(use-package corfu
  :ensure t
  :custom
  (corfu-auto t)
  (corfu-auto-delay 1)
  (corfu-exclude-modes '(shell-mode eshell-mode comint-mode))
  :init
  (global-corfu-mode)
  :hook ((shell-mode . corfu-echo-mode)
         (eshell-mode . corfu-echo-mode)
         (comint-mode . corfu-echo-mode)))

(use-package cape
  :ensure t
  :init
  (add-hook 'completion-at-point-functions #'cape-dabbrev)
  (add-hook 'completion-at-point-functions #'cape-file))

(use-package dtrt-indent
  :ensure t
  :init
  (dtrt-indent-global-mode))

(use-package treesit-auto
  :ensure t
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(use-package sqlformat
  :ensure t
  :config
  (setq sqlformat-command 'pgformatter)
  (setq sqlformat-args '("-s2" "-g")))

;; Set identity.
(setq user-full-name "Evan Black"
      user-mail-address "evan@erblack.com")

;; Set custom binds.
(global-set-key (kbd "C-x K") 'kill-current-buffer)

(defun jq-on-region ()
  (interactive)
  (shell-command-on-region (region-beginning) (region-end) "jq ." nil t))
(global-set-key (kbd "C-c j") 'jq-on-region)

;; Configure Mac-specific things.
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
